#!/bin/bash

sudo groupmod --gid $HOST_GID build \
    && usermod --uid $HOST_UID --gid $HOST_GID build

sudo chown -R $HOST_UID:$HOST_GID /home/build /build/project /build/cache
