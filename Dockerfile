FROM registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder-base:bullseye

USER root

RUN apt -y install vim screen xterm

COPY entrypoint.sh /usr/bin/
ENTRYPOINT ["/usr/bin/entrypoint.sh"]

WORKDIR /build/project
