all: push

build: Dockerfile Dockerfile_base
	docker build -f Dockerfile_base -t registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder-base:bullseye ./
	docker build -f Dockerfile -t registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder:bullseye ./

push: build
	docker push registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder-base:bullseye
	docker push registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder:bullseye
