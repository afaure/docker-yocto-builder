# Dockerfiles to build yocto images

## Usage :
- Add to zshrc :
`
export YOCTO_DL_DIR=/home/path/dl_dir
export YOCTO_SSTATE_CACHE_DIR=/home/path/sstate_cache
alias yocto='docker rm yocto_builder && \
            docker run --name yocto_builder -it \
                -v ${YOCTO_SSTATE_CACHE_DIR}:/build/cache/yocto_sstate_cache \
                -v ${YOCTO_DL_DIR}:/build/cache/yocto_dl_dir \
                -v `pwd`:/build/project \
                -v ${HOME}/.ssh:/root/.ssh \
                registry.gitlab.com/afaure/docker-yocto-builder/docker-yocto-builder:bullseye'
`

Go to your Yocto project, and run `yocto` to launch the docker container
